package pl.bernacki.streamoptional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamOptionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamOptionalApplication.class, args);
	}
}
