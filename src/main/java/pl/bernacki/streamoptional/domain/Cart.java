package pl.bernacki.streamoptional.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import pl.bernacki.streamoptional.external.mongo.CartMongo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Builder
@Value
@AllArgsConstructor
public class Cart {
    List<Integer> itemIds;


    Cart(){
        this.itemIds = new ArrayList<>();
    }

    public static Cart createFrom(CartMongo cartMongo) {
        return Cart.builder()
            .itemIds(
                cartMongo.getItems().entrySet()
                    .stream()
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList())
            )
            .build();
    }
}
