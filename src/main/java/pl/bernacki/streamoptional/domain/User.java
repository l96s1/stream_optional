package pl.bernacki.streamoptional.domain;

import lombok.Builder;
import lombok.Value;
import pl.bernacki.streamoptional.external.mongo.CartMongo;
import pl.bernacki.streamoptional.external.mongo.UserMongo;

import static pl.bernacki.streamoptional.domain.User.UserType.ADMINISTRATOR;
import static pl.bernacki.streamoptional.domain.User.UserType.EDITOR;

@Value
@Builder
public class User {
    String login;

    UserType userType;

    Cart cart;

    public enum UserType {
        ADMINISTRATOR, USER, EDITOR
    }

    public static User create(UserMongo userMongo) {
        return User.builder()
            .login(userMongo.getLogin())
            .userType(
                UserType.valueOf(
                    userMongo.getType().orElse("EDITOR")
                )
            )
            .cart(
                Cart.createFrom(
                    userMongo.getCart().orElseGet(CartMongo.builder()::build)
                )
            )
            .build();
    }

    public boolean isAdministrator() {
        return userType == ADMINISTRATOR;
    }

    public boolean isEditor() {
        return userType == EDITOR;
    }

}
