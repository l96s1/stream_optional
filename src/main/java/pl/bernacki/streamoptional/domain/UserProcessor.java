package pl.bernacki.streamoptional.domain;

import java.util.List;
import java.util.stream.Collectors;

public class UserProcessor {
    public List<Double> sameList(List<Double> list) {
        return list.stream()
            .map(Math::sqrt)
            .map(item -> Math.pow(item, 2))
            .collect(Collectors.toList());
    }

    public List<User> getUsersWhereSecondLetterOfLoginIsA(List<User> list) {

        return list.stream()
            .filter(user -> stringHaveAOnSeconPosition(user.getLogin()))
            .collect(Collectors.toList());
    }

    private boolean stringHaveAOnSeconPosition(String string) {
        return string.charAt(1) == 'a';
    }

    public Boolean containOnlyAdmins(List<User> list) {
        return list.stream()
            .allMatch(User::isAdministrator);
    }

    public List<User> returnOnlyTwoEditors(List<User> list){
        return list.stream()
            .filter(User::isEditor)
            .limit(2)
            .collect(Collectors.toList());
    }
}
