package pl.bernacki.streamoptional.external.mongo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Builder
@Value
@AllArgsConstructor
public class CartMongo {
    Map<Integer, String> items;

}
