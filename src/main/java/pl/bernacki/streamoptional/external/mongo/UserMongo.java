package pl.bernacki.streamoptional.external.mongo;

import lombok.Builder;
import lombok.Value;

import java.util.Optional;

@Builder
@Value
public class UserMongo {
    String dataBaseId;
    String login;
    Optional<String> type;
    Optional<CartMongo> cart;
}
