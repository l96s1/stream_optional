package pl.bernacki.streamoptional.domain

import pl.bernacki.streamoptional.external.mongo.CartMongo
import pl.bernacki.streamoptional.external.mongo.UserMongo
import spock.lang.Specification

class UserSpec extends Specification {

    def "should set user as editor when type is not provided"() {
        given:
        def userMongo = UserMongo.builder()
            .dataBaseId("22")
            .login("example login")
            .cart(Optional.of(CartMongo.builder().items([22: "krzeslo", 26: "fotel"]).build()))
            .type(Optional.empty())
            .build()

        when:
        def user = User.create(userMongo)

        then:
        user.userType.equals(User.UserType.EDITOR)
    }

}
