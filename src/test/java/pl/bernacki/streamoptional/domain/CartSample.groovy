package pl.bernacki.streamoptional.domain

class CartSample {
    private static final DEFAULT_PROPERTIES = [
        itemIds: [22, 33, 44, 55, 67]
    ]

    static Cart sampleCart(customProporties = [:]) {
        Map<String, Object> properties = DEFAULT_PROPERTIES + customProporties
        return Cart.builder()
            .itemIds(
            (List<Integer>) properties.itemIds
            )
            .build()
    }
}
