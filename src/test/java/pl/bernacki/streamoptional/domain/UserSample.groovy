package pl.bernacki.streamoptional.domain

import static pl.bernacki.streamoptional.domain.CartSample.sampleCart

class UserSample {

    private static final DEFAULT_PROPERTIES = [
        login   : "userLogin",
        userType: (User.UserType.USER),
        cart    : sampleCart()
    ]

    static User sampleUser(customProporties = [:]) {
        Map<String, Object> properties = DEFAULT_PROPERTIES + customProporties
        return User.builder()
            .login((String) properties.login)
            .userType((User.UserType) properties.userType)
            .cart((Cart) properties.cart)
            .build()
    }
}
