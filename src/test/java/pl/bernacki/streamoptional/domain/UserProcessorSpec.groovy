package pl.bernacki.streamoptional.domain

import spock.lang.Unroll

import static pl.bernacki.streamoptional.domain.UserSample.sampleUser

class UserProcessorSpec extends spock.lang.Specification {

    def userProcessor = new UserProcessor()

    def "SameList"() {
        given:
        List<Double> list = [27d, 30d, 4d, 9d]

        when:
        def result = userProcessor.sameList(list)

        then:

        result.equals(list)
    }

    def "GetUsersWhereSecondLetterOfLoginIsA"() {
        given:
        List<User> users = [sampleUser(login: "franek"), sampleUser(login: "anna"), sampleUser(login: "krzysztof"), sampleUser(login: "marek")]

        when:
        def result = userProcessor.getUsersWhereSecondLetterOfLoginIsA(users)

        then:
        result.size() == 1
    }

    def "ContainOnlyAdmn list have only admins"() {
        given:
        List<User> users = [
            sampleUser(userType: User.UserType.ADMINISTRATOR),
            sampleUser(userType: User.UserType.ADMINISTRATOR),
            sampleUser(userType: User.UserType.ADMINISTRATOR),
            sampleUser(userType: User.UserType.ADMINISTRATOR)
        ]

        when:
        def result = userProcessor.containOnlyAdmins(users)

        then:
        result
    }

    @Unroll
    def "should return false when there are users who are not admins "() {

        when:
        def result = userProcessor.containOnlyAdmins(users)

        then:
        !result

        where:
        users << [
            [
                sampleUser(userType: User.UserType.ADMINISTRATOR),
                sampleUser(userType: User.UserType.EDITOR),
                sampleUser(userType: User.UserType.USER),
                sampleUser(userType: User.UserType.ADMINISTRATOR)
            ],
            [
                sampleUser(userType: User.UserType.ADMINISTRATOR),
                sampleUser(userType: User.UserType.ADMINISTRATOR),
                sampleUser(userType: User.UserType.EDITOR),
                sampleUser(userType: User.UserType.ADMINISTRATOR)
            ]
        ]
    }

    @Unroll
    def "ReturnOnlyTwoEditors"() {
        when:
        def result = userProcessor.returnOnlyTwoEditors(users)

        then:
        result.size() <= 2
        result.every() {item -> item.userType == User.UserType.EDITOR}

        where:
        users << [
            [
                sampleUser(userType: User.UserType.ADMINISTRATOR),
                sampleUser(userType: User.UserType.EDITOR),
                sampleUser(userType: User.UserType.USER),
                sampleUser(userType: User.UserType.ADMINISTRATOR)
            ],
            [
                sampleUser(userType: User.UserType.ADMINISTRATOR),
                sampleUser(userType: User.UserType.ADMINISTRATOR),
                sampleUser(login:"first", userType: User.UserType.EDITOR),
                sampleUser(login:"second", userType: User.UserType.EDITOR),
                sampleUser(login:"third", userType: User.UserType.EDITOR),
                sampleUser(userType: User.UserType.ADMINISTRATOR)
            ]
        ]
    }
}
